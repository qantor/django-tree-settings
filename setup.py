

from setuptools import setup, find_packages

setup(
    name='django_tree_settings',
    version='0.1',
    author='',
    author_email='',
    packages=find_packages(),
    include_package_data=True,
    url='',
    license='See LICENSE.txt',
    description='',
    long_description=open('README.txt').read(),
)
