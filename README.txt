====================
django-tree-settings
====================

**Tree settings for YourModel.**

Model must contains ``data = JSONField(default={..})``

**Installation:**

1. Add ``django_tree_settings`` to INSTALLED_APPS.
2. Migrate. ``./manage.py migrate django_tree_settings``
3. Add SettingsModelMixin from django_tree_settings.mixins to YourModel:

``class YourModel(SettingsModelMixin, model.Models):``

4. Add SettingsSerializerMixin to YourModelUpdateSerializer:

``class YourModelUpdateSerializer(SettingsSerializerMixin, serializers.ModelSerializer):``