from rest_framework import serializers

from django_tree_settings.models import TreeSetting


class RecursiveField(serializers.Serializer):
    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class TreeSettingSerializer(serializers.ModelSerializer):
    children = RecursiveField(many=True, read_only=True)
    id = serializers.SerializerMethodField()

    class Meta:
        model = TreeSetting
        fields = ('id', 'name', 'verbose_name', 'type', 'parent', 'children', 'blank', 'default')

    def get_id(self, obj):
        return str(obj.pk)
