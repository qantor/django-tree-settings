from django.conf import settings
from rest_framework.viewsets import ReadOnlyModelViewSet

from django_tree_settings.models import TreeSetting
from django_tree_settings.serializers import TreeSettingSerializer


class Settings(ReadOnlyModelViewSet):
    serializer_class = TreeSettingSerializer

    def get_queryset(self):
        model = self.request.query_params.get('model')
        return TreeSetting.objects.filter(
            model__content_type__model=model,
            parent=None,
            language=settings.LANGUAGE_CODE,
        )
