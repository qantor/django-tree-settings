from django.conf.urls import url

from django_tree_settings.views import Settings

urlpatterns = [
    url(r'^tree/', Settings.as_view({'get': 'list'}), name='settings-list'),
]