from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from rest_framework.exceptions import ValidationError
from django_tree_settings.models import TreeSetting


def validate_settings(instance, attr):
    values = attr.get('settings')
    values = {str(k): v for k, v in values.items()}
    res = values.copy()
    ct = ContentType.objects.get_for_model(instance)
    settings_format = {str(s.pk): s for s in TreeSetting.objects.filter(model__content_type=ct,
                                                                        language=settings.LANGUAGE_CODE)}
    for k, v in values.items():
        if k not in settings_format.keys():
            del res[k]
            continue
        setting = settings_format[k]
        if v == setting.default:
            continue
        elif v:
            if setting.type == 'group':
                del res[k]
                continue
            validator = SETTING_VALIDATOR[setting.type]
            try:
                validator(v)
            except ValidationError as e:
                raise ValidationError("Invalid value for k: %s : %s" % (k, e))

        else:
            if setting.value_required:
                raise ValidationError('Value required for setting {}'.format(k))

    data_settings = instance.data.get('settings')
    data_settings.update(res)

    for k, v in settings_format.items():
        if v.value_required:
            if data_settings.get(k) is None:
                data_settings[k] = v.default

    return data_settings

def validate_int(value):
    try:
        int(value)
    except Exception as e:
        raise ValidationError("Incorrect value for int: %s" % e)


def validate_float(value):
    try:
        float(value)
    except Exception as e:
        raise ValidationError("Incorrect value for float: %s" % e)


def validate_str(value):
    pass


def validate_bool(value):
    if value.lower() in ('true', 'false'):
        pass
    else:
        raise ValidationError("Incorrect value for bool: %s. "
                              "Should be something like 'true' or 'false"
                              % value)


def validate_color(value):
    try:
        int(value, 16)
    except Exception as e:
        raise ValidationError("Incorrect value for color: %s" % e)


SETTING_VALIDATOR = {
    'int': validate_int,
    'float': validate_float,
    'str': validate_str,
    'bool': validate_bool,
    'color': validate_color,
}
