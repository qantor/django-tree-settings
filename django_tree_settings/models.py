from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import models, transaction
from django.utils.translation import ugettext_lazy as _


SETTING_TYPES = (
    ('int', _('Integer')),
    ('float', _('Float')),
    ('str', _('String')),
    ('bool', _('Boolean')),
    ('color', _('Color')),
    ('group', _('Group')),
)

class ModelTreeSetting(models.Model):
    name = models.CharField(max_length=255, blank=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name='settings_models')

    def __str__(self):
        return self.name


class TreeSetting(models.Model):
    name = models.CharField(max_length=255)
    verbose_name = models.CharField(max_length=255)
    type = models.CharField(max_length=255, choices=SETTING_TYPES)
    parent = models.ForeignKey('self', related_name='children', on_delete=models.CASCADE, blank=True, null=True)

    blank = models.BooleanField(default=False)
    default = models.TextField(blank=True)
    model = models.ForeignKey(ModelTreeSetting, on_delete=models.CASCADE, related_name='settings')
    sort = models.IntegerField(default=0)
    language = models.CharField(default=settings.LANGUAGE_CODE, max_length=8)

    class Meta:
        ordering = ('sort', )

    def save(self, *args, **kwargs):
        from .tasks import update_settings
        if self.pk is None:
            transaction.on_commit(
                lambda: update_settings.delay(self.id)
            )
        super().save(*args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        from .tasks import delete_settings
        delete_settings.delay(self.id)

    def __str__(self):
        return self.name

    @property
    def value_required(self):
        return self.type != 'group' and not self.blank

