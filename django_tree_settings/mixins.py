from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from rest_framework.exceptions import ValidationError

from django_tree_settings.models import TreeSetting
from django_tree_settings.utils import validate_settings


class SettingsModelMixin(object):
    @classmethod
    def generate_settings(cls):
        ct = ContentType.objects.get_for_model(cls)
        return {
            "settings":
                {str(x.pk): x.default for x in TreeSetting.objects.filter(
                    model__content_type=ct, blank=False, language=settings.LANGUAGE_CODE)}
        }

    def save(self, *args, **kwargs):
        if self.pk is None:
            try:
                self.data.update(self.generate_settings())
            except AttributeError:
                raise ValidationError('No "data" field in model %s' % self._meta.model)
        super().save(*args, **kwargs)


class SettingsSerializerMixin(object):

    def validate(self, attrs):
        if attrs.get('settings'):
            validated_settings = validate_settings(self.instance, attrs)
            old_data = self.instance.data.copy()
            old_data['settings'] = validated_settings
            attrs['data'] = old_data
            attrs['settings'] = validated_settings
        return super().validate(attrs)
