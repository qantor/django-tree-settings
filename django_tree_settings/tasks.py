from celery import shared_task
from django.db import transaction

from django_tree_settings.models import TreeSetting


@shared_task
def update_settings(setting_id):
    setting = TreeSetting.objects.get(pk=setting_id)

    if not setting.value_required:
        return

    objs = setting.model.content_type.model_class().objects.all()
    for obj in objs:
        data_for_change = obj.data.get('settings', {})
        if data_for_change.get(str(setting.id)) is None and setting.blank is False:
            data_for_change[str(setting.id)] = setting.default
            obj.data.update({'settings': data_for_change})
            obj.save()


@shared_task
def delete_settings(setting_id):
    with transaction.atomic():
        setting = TreeSetting.objects.get(pk=setting_id)

        objs = setting.model.content_type.model_class().objects.all()
        for obj in objs:
            data_for_change = obj.data.get('settings', {})
            if data_for_change.get(str(setting.id)):
                del data_for_change[str(setting_id)]
                obj.data.update({'settings': data_for_change})
                obj.save()

        super(TreeSetting, setting).delete()
