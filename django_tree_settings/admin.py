from django.contrib import admin

from django_tree_settings.models import TreeSetting, ModelTreeSetting


@admin.register(TreeSetting)
class TreeSettingAdmin(admin.ModelAdmin):
    list_display = ('name', 'verbose_name', 'type', 'parent', 'blank', 'model',
                    'language', 'sort')
    list_editable = ('verbose_name', 'sort')


@admin.register(ModelTreeSetting)
class ModelTreeSettingAdmin(admin.ModelAdmin):
    list_display = ('name', 'content_type')
